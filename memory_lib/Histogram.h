#ifndef HISTOGRAM_H_
#define HISTOGRAM_H_

#include <vector>
#include <iostream>

/// A Histogram class. The bins and counts are of type 64-bit unsigned integer.
/// For a more general histogram class, the bin type could be templated.
class Histogram
{
public:
	Histogram() : num_bins_(0) {}

	/// \brief Constructor that takes the bin borders as input.
	/// Infers the number of bins and creates the vector to hold the histogram data.
	/// \param bins The bin borders. The bins must be in ascending order.
	Histogram(const std::vector<uint64_t>& bins)
	{
		bins_ = bins;
		num_bins_ = bins.size() - 1;
		histogram_ = std::move(std::vector<uint64_t>(num_bins_, 0));
	}

	/// \brief Sets the bins.
	/// Infers the number of bins and creates the vector for the histogram data.
	/// If any histogram data that existed before this call will be erased.
	/// \param bins The bins - must be in asecending order.
	void SetBins(const std::vector<uint64_t>& bins)
	{
		bins_ = bins;
		num_bins_ = bins.size() - 1;
		histogram_ = std::move(std::vector<uint64_t>(num_bins_, 0));
	}

	/// \brief Gets the number of bins.
	/// \returns Number of bins
	uint64_t GetNumBins() const
	{
		return num_bins_;
	}

	/// \brief Updates the histogram for the given value to be counted.
	/// Increments the value of the appropriate bin for the given value.
	/// \param val The value to counted.
	void UpdateAddValue(uint64_t val)
	{
		try
		{
			uint64_t hist_val = histogram_[GetBinIndex(val)];
			histogram_[GetBinIndex(val)] = std::max(hist_val, hist_val + 1); // To account for INT_MAX + 1
		}
		catch(const std::exception &e)
		{
			std::cerr << e.what() << std::endl;
		}
	}

	/// \brief Updates the histogram for the given value to be removed.
	/// Decrements the value of the appropriate bin for the given value.
	/// \param val The value to removed.
	void UpdateRemoveValue(uint64_t val)
	{
		try
		{
			uint64_t hist_val = histogram_[GetBinIndex(val)];
			histogram_[GetBinIndex(val)] = std::min(hist_val, hist_val - 1); // To account for 0 - 1
		}
		catch(const std::exception &e)
		{
			std::cerr << e.what() << std::endl;
		}
	}

	/// Sets the value of all bins to zero.
	void ResetCounts()
	{
		histogram_.assign(histogram_.size(), 0);
	}

	/// Prints a representation of the count in a bin.
	/// Prints a representation using the '#' character. Each '#' represents a count of [1, count_per_graduation]
	/// \param os Stream to which to write/print.
	/// \param bin_idx The bin, which count to write to the stream.
	/// \param count_per_graduation The max amount each '#' represents.
	std::ostream& ToOstream(std::ostream& os, const uint64_t bin_idx, const uint64_t count_per_graduation) const
	{
		uint64_t num_graduations = static_cast<uint64_t>(histogram_[bin_idx] / count_per_graduation);
		num_graduations += ((histogram_[bin_idx] % count_per_graduation) > 0)? 1 : 0;

		for(;num_graduations > 0; num_graduations--)
		{
			os << '#';
		}

		return os;
	}

private:
	/// Gets the index of the bin, in the histogram vector, the given value will fall into.
	/// This works for any bins_, as long as it is in ascending order, with non-repeating values. The runtime for this is O(log n).
	/// For if the bins_ to be used are known and follow a deterministic, trivial equation/pattern,
	/// then it is possible to have a runtime of O(1). For example, this case - the allocations bins are 2^n, and the age 10^n.
	/// \param val Value for which to find the bin.
	/// \returns The bin index in the histogram for val.
	/// \throws runtime_error if the value does not fall into any of the bins.
	uint64_t GetBinIndex(const uint64_t val) const
	{
		auto next_bin_iter = std::lower_bound(bins_.begin(), bins_.end(), val);

		if(next_bin_iter == bins_.end())
		{
			throw std::runtime_error("Value out of range.");
		}

		uint64_t next_bin_idx = std::distance(bins_.begin(), next_bin_iter);

		return std::min(next_bin_idx - 1, next_bin_idx); // To account for 0-1
	}

	uint64_t num_bins_;
	std::vector<uint64_t> bins_;
	std::vector<uint64_t> histogram_;
};
#endif
