#ifndef POINTER_INFO_H_
#define POINTER_INFO_H_
#include <chrono>

using steady_clock_time = std::chrono::time_point<std::chrono::steady_clock>;
using steady_clock = std::chrono::steady_clock;

/// Struct to hold pointer information - the size allocated for the pointer and the time of allocation.
struct PointerInfo
{
	PointerInfo() : size(0) {}
	PointerInfo(uint64_t size_, const steady_clock_time& alloc_time_ = steady_clock::now() )
	{
		alloc_time = alloc_time_;
		size = size_;
	}

	uint64_t size;
	steady_clock_time alloc_time;
};
#endif
