LD_PRELOAD-able library to override malloc, realloc, calloc, free.

Author: Valmiki Rampersad
E-mail: valmiki.rampersad@gmail.com

Tools used:
* Make 4.1
* g++ 7.3.0
* Catch2 (included. For testing - software must be tested.)

Running 'make' or 'make all' will build everything - library, unit tests, and a test-main.

To build the library only, run 'make library'. The library output file is 'mem_alloc_stats.so'.

To build the unit tests and test-main, run 'make tests'. This will build 2 executables:
* test_main - a simple application that does different allocations that calls SIGQUIT at various points. Run this with the LD_PRELOAD. or run the script 'test_main_with_ld_preload.sh'.
* mem_alloc_stats_tests - the Catch2 unit test executable. Do not need to run this with LD_PRELOAD (not sure what will happen if you do.)

Files for the library:
* mem_alloc_stats.cc
* MemoryAllocatorWithStats.h
* SystemMemoryAllocator.h
* PointerInfo.h
* Histogram.h

Files for the unit tests:
* Tests.cc
* MemoryAllocatorWithStatsTests.h
* HistogramTests.h

Files for the test-main:
* test_main.cc
* test_main_with_ld_preload.sh

