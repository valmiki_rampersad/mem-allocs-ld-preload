#ifndef MEMORY_ALLOCATOR_WITH_STATS_TESTS_H_
#define MEMORY_ALLOCATOR_WITH_STATS_TESTS_H_
//#define CATCH_CONFIG_MAIN
#include "catch2/catch.hpp"

#include <chrono>
#include <ratio>
#include <algorithm>
#include <sstream>
#include <string>
#include <thread>

// A useful hack for testing.
#define private public
#include "SystemMemoryAllocator.h"
#include "MemoryAllocatorWithStats.h"
#define private private

TEST_CASE("SystemMemoryAllocator dlfncs are successful.", "[SystemMemoryAllocator]")
{
	SystemMemoryAllocator sys_mem_alloc;

	REQUIRE(sys_mem_alloc.system_malloc);
	REQUIRE(sys_mem_alloc.system_realloc);
	REQUIRE(sys_mem_alloc.system_calloc);
	REQUIRE(sys_mem_alloc.system_free);
}


SCENARIO("Counts the number of allocations from start.", "[MemAllocWithStats]")
{
	GIVEN("A new MemoryAllocatorWithStats object"){
		MemoryAllocatorWithStats mem_alloc_stats;

		REQUIRE(mem_alloc_stats.GetTotalNumAllocations() == 0);

		WHEN("malloc is called")
		{
			char* a = reinterpret_cast<char*>(mem_alloc_stats.malloc(sizeof(char)));
			mem_alloc_stats.free(a);

			THEN("the total number of allocations is incremented")
			{
				REQUIRE(mem_alloc_stats.GetTotalNumAllocations() == 1);
			}
		}

		WHEN("calloc is called")
		{
			char* a = reinterpret_cast<char*>(mem_alloc_stats.calloc(4, sizeof(char)));
			mem_alloc_stats.free(a);

			THEN("the total number of allocations is incremented")
			{
				REQUIRE(mem_alloc_stats.GetTotalNumAllocations() == 1);
			}
		}

		WHEN("realloc is called")
		{
			char* a = reinterpret_cast<char*>(mem_alloc_stats.malloc(sizeof(char)));
			uint32_t* b = reinterpret_cast<uint32_t*>(mem_alloc_stats.realloc(a, sizeof(uint32_t)));
			mem_alloc_stats.free(b);

			THEN("the total number of allocations is incremented")
			{
				REQUIRE(mem_alloc_stats.GetTotalNumAllocations() == 2);
			}
		}
	}
}


SCENARIO("Gives the total current memory allocation", "[MemAllocWithStats]")
{
	GIVEN("A new MemoryAllocatorWithStats object")
	{
		MemoryAllocatorWithStats mem_alloc_stats;
		REQUIRE(mem_alloc_stats.GetCurrentMemoryAllocation() == 0);

		WHEN("allocations occur")
		{
			uint64_t* num_8bytes = reinterpret_cast<uint64_t*>(mem_alloc_stats.malloc(sizeof(uint64_t)));

			THEN("the total current memory allocation increases")
			{
				REQUIRE(mem_alloc_stats.GetCurrentMemoryAllocation() == sizeof(uint64_t));

				uint32_t* num_4bytes = reinterpret_cast<uint32_t*>(mem_alloc_stats.malloc(sizeof(uint32_t)));
				REQUIRE(mem_alloc_stats.GetCurrentMemoryAllocation() == (sizeof(uint64_t) + sizeof(uint32_t)));

				WHEN("deallocations occur")
				{
					mem_alloc_stats.free(num_8bytes);
					THEN("the total current memory allocation decreases")
					{
						REQUIRE(mem_alloc_stats.GetCurrentMemoryAllocation() == sizeof(uint32_t));

						mem_alloc_stats.free(num_4bytes);
						REQUIRE(mem_alloc_stats.GetCurrentMemoryAllocation() == 0);
					}
				}
			}
		}
	}
}


SCENARIO("Tracks individual pointer allocations", "[MemAllocWithStats]")
{
	GIVEN("A new MemoryAllocatorWithStats object")
	{
		MemoryAllocatorWithStats mem_alloc_stats;

		WHEN("after a malloc allocation")
		{
			int* a = reinterpret_cast<int*>(mem_alloc_stats.malloc(sizeof(int)));

			THEN("the internal map will record the pointer's allocation")
			{
				REQUIRE(mem_alloc_stats.mem_allocs_[a].size == sizeof(*a));

				WHEN("pointer is freed")
				{
					mem_alloc_stats.free(a);

					THEN("the internal map should not contain the pointer")
					{
						REQUIRE(mem_alloc_stats.mem_allocs_.find(a) == mem_alloc_stats.mem_allocs_.end());
					}
				}
			}
		}

		WHEN("after a realloc allocation")
		{
			char* a = reinterpret_cast<char*>(mem_alloc_stats.malloc(sizeof(char)));
			int* b = reinterpret_cast<int*>(mem_alloc_stats.realloc(a, sizeof(int)));

			THEN("the internal map will record the pointer's allocation")
			{
				REQUIRE(mem_alloc_stats.mem_allocs_[b].size == sizeof(*b));

				WHEN("pointer is freed")
				{
					mem_alloc_stats.free(b);

					THEN("the internal map should not contain the pointer")
					{
						REQUIRE(mem_alloc_stats.mem_allocs_.find(b) == mem_alloc_stats.mem_allocs_.end());
					}
				}
			}
		}

		WHEN("after a calloc allocation")
		{
			char* a = reinterpret_cast<char*>(mem_alloc_stats.calloc(10, sizeof(char)));

			THEN("the internal map will record the pointer's allocation")
			{
				REQUIRE(mem_alloc_stats.mem_allocs_[a].size == (sizeof(*a) * 10));

				WHEN("pointer is freed")
				{
					mem_alloc_stats.free(a);

					THEN("the internal map should not contain the pointer")
					{
						REQUIRE(mem_alloc_stats.mem_allocs_.find(a) == mem_alloc_stats.mem_allocs_.end());
					}
				}
			}
		}
	}
}


SCENARIO("Tracks individual pointer age", "[MemAllocWithStats]")
{
	GIVEN("A new MemoryAllocatorWithStats object")
	{
		MemoryAllocatorWithStats mem_alloc_stats;

		WHEN("after an allocation")
		{
			int* a = reinterpret_cast<int*>(mem_alloc_stats.malloc(sizeof(int)));
			auto approx_alloc_time = std::chrono::steady_clock::now();

			THEN("the internal map will record the pointer's time of birth")
			{
				REQUIRE((approx_alloc_time - mem_alloc_stats.mem_allocs_[a].alloc_time) < std::chrono::microseconds(100));

			}
		}
	}
}


SCENARIO("Keeps a running histogram of allocations", "[MemAllocWithStats]")
{
	GIVEN("A new MemoryAllocatorWithStats object")
	{
		MemoryAllocatorWithStats mem_alloc_stats;

		WHEN("after an allocation")
		{
			const uint32_t kNumElements = 10;
			int* a = reinterpret_cast<int*>(mem_alloc_stats.calloc(kNumElements, sizeof(int)));

			THEN("the appropriate bin in the internal histogram will be incremented")
			{
				auto hist_bin_idx = mem_alloc_stats.mem_allocs_hist_.GetBinIndex(kNumElements * sizeof(int));
				REQUIRE(mem_alloc_stats.mem_allocs_hist_.histogram_[hist_bin_idx] == 1);

				WHEN("after a deallocation")
				{
					mem_alloc_stats.free(a);

					THEN("the appropriate bin in the internal histogram will be decremented")
					{
						REQUIRE(mem_alloc_stats.mem_allocs_hist_.histogram_[hist_bin_idx] == 0);
					}
				}
			}
		}
	}
}


// A test program. There are no assertion tests in this.
// This is a slow test - so hidden from default run.
TEST_CASE("TestApp1", "[.][TestApp]")
{
	MemoryAllocatorWithStats mem_alloc_stats;

	std::cout << "int_a = malloc int" << std::endl;
	int* a = (int*)mem_alloc_stats.malloc(sizeof(int));

	std::cout << "sleep 2s" << std::endl;
	std::this_thread::sleep_for(std::chrono::seconds(2));

	std::cout << mem_alloc_stats << std::endl;

	std::cout << "char_b = realloc a to char" << std::endl;
	char* b = (char*)mem_alloc_stats.realloc(a, sizeof(char));

	std::cout << mem_alloc_stats << std::endl;

	std::cout << "free char_b" << std::endl;
	mem_alloc_stats.free(b);

	std::cout << mem_alloc_stats << std::endl;

	std::cout << "int4_c = calloc int, 4x" << std::endl;
	int* c = (int*)mem_alloc_stats.calloc(4, sizeof(int));

	std::cout << mem_alloc_stats << std::endl;

	std::cout << "free int4_c" << std::endl;
	mem_alloc_stats.free(c);

	std::cout << mem_alloc_stats << std::endl;

}

// This is a slow test - so hidden from default run.
TEST_CASE("TestApp2", "[.][TestApp]")
{
	MemoryAllocatorWithStats mem_alloc_stats;

	std::cout << "char_mallocs = malloc 100000 char" << std::endl;
	std::cout << "b128_mallocs = malloc 100000 128-bytes" << std::endl;
	const int kMallocsCount = 25000;

	std::vector<void*> char_mallocs;
	char_mallocs.reserve(kMallocsCount);

	std::vector<void*> b128_mallocs;
	b128_mallocs.reserve(kMallocsCount);

	for(int i = 1; i <= kMallocsCount; i++)
	{
		char_mallocs.push_back(mem_alloc_stats.malloc(sizeof(char)));
		b128_mallocs.push_back(mem_alloc_stats.malloc(128));

		std::this_thread::sleep_for(std::chrono::milliseconds(1));

		if(i % 1000 == 0)
		{
			std::cout << mem_alloc_stats << std::endl;
		}
	}

	std::cout << "free char_mallocs" << std::endl;
	std::cout << "free b128_mallocs" << std::endl;
	for(int i = 1; i <= kMallocsCount; i++)
	{
		mem_alloc_stats.free(char_mallocs[i-1]);
		mem_alloc_stats.free(b128_mallocs[i-1]);

		std::this_thread::sleep_for(std::chrono::milliseconds(1));

		if(i % 1000 == 0)
		{
			std::cout << mem_alloc_stats << std::endl;
		}
	}

}
#endif
