#ifndef HISTOGRAM_TESTS_H_
#define HISTOGRAM_TESTS_H_
#include "catch2/catch.hpp"

#include <algorithm>
#include <sstream>
#include <string>

// A useful hack for testing.
#define private public
#include "Histogram.h"
#define private private

SCENARIO("Histogram, given the bin borders, infers the correct number of bins", "[Histogram]")
{
	WHEN("A Histogram is created with default constructor")
	{
		Histogram hist;

		THEN("The number of bins is zero")
		{
			REQUIRE(hist.num_bins_ == 0);
		}

		THEN("The number of bins is zero")
		{
			REQUIRE(hist.histogram_.size() == 0);
		}
	}

	WHEN("A Histogram initialized with bin borders")
	{
		Histogram hist({0, 1, 2}); // 3 borders => 2 bins

		THEN("the number of bins is inferred")
		{
			REQUIRE(hist.num_bins_ == 2);
		}

		THEN("the histogram space is created with the correct number of bins")
		{
			REQUIRE(hist.histogram_.size() == 2);
		}

		THEN("the histogram bin values are all 0")
		{
			REQUIRE(std::all_of(hist.histogram_.begin(), hist.histogram_.end(), [](uint64_t i){ return i == 0; }));
		}
	}

	GIVEN("A Histogram")
	{
		Histogram hist;

		WHEN("The bins are set")
		{
			hist.SetBins({1, 2, 3});

			THEN("the histogram space is created with the correct number of bins")
			{
				REQUIRE(hist.histogram_.size() == 2);
			}

			THEN("the histogram bin values are all 0")
			{
				REQUIRE(std::all_of(hist.histogram_.begin(), hist.histogram_.end(), [](uint64_t i){ return i == 0; }));
			}
		}
	}
}


SCENARIO("Updating the histogram", "[Histogram]")
{
	GIVEN("A Histogram initialized with bins {0, 2, 4}")
	{
		Histogram hist({0, 2, 4});

		WHEN("Adding a value of 0 to the histogram")
		{
			hist.UpdateAddValue(0);

			THEN("Histogram should be [1, 0]")
			{
				REQUIRE(hist.histogram_ == std::vector<uint64_t>({1, 0}));
			}
		}

		WHEN("Adding a value of 2 to the histogram")
		{
			hist.UpdateAddValue(2);

			THEN("Histogram should be [1, 0]")
			{
				REQUIRE(hist.histogram_ == std::vector<uint64_t>({1, 0}));
			}
		}

		WHEN("Adding a value of 3 to the histogram")
		{
			hist.UpdateAddValue(3);

			THEN("Histogram should be [0, 1]")
			{
				REQUIRE(hist.histogram_ == std::vector<uint64_t>({0, 1}));
			}
		}

		WHEN("Adding a value of 4 to the histogram")
		{
			hist.UpdateAddValue(4);

			THEN("Histogram should be [0, 1]")
			{
				REQUIRE(hist.histogram_ == std::vector<uint64_t>({0, 1}));
			}

			WHEN("Removing a value of 4 from the histogram")
			{
				hist.UpdateRemoveValue(4);

				THEN("Histogram should be [0, 0]")
				{
					REQUIRE(hist.histogram_ == std::vector<uint64_t>({0, 0}));
				}
			}
		}

		WHEN("Removing a value (3) from an already empty bin")
		{
			hist.UpdateRemoveValue(3);

			THEN("Histogram should be [0, 0]")
			{
				REQUIRE(hist.histogram_ == std::vector<uint64_t>({0, 0}));
			}
		}

		WHEN("Adding a value outside the range of the bins")
		{
			hist.UpdateAddValue(5);

			THEN("Histogram should be [0,0")
			{
				REQUIRE(hist.histogram_ == std::vector<uint64_t>({0, 0}));
			}
		}
	}
}


SCENARIO("Printing the histogram data", "[Histogram]")
{
	GIVEN("A Histogram initialized with bins {0, 2, 4}. And using count per graduation of 3")
	{
		Histogram hist({0, 2, 4});

		const uint64_t kCountPerGrad = 3;

		WHEN("the histogram is empty, i.e. no value has been added")
		{
			THEN("nothing should be printed")
			{
				REQUIRE(hist.histogram_ == std::vector<uint64_t>({0, 0}));

				for(uint32_t i = 0; i < hist.histogram_.size(); i++)
				{
					std::stringstream ss;
					hist.ToOstream(ss, 0, kCountPerGrad);
					REQUIRE(ss.str() == "");
				}
			}
		}

		WHEN("a value of 1 is added to the histogram")
		{
			hist.UpdateAddValue(1);
			THEN("the first bin should have a value of one and a single # should print for that bin")
			{
				REQUIRE(hist.histogram_ == std::vector<uint64_t>({1, 0}));

				std::stringstream ss;

				hist.ToOstream(ss, 0, kCountPerGrad);
				REQUIRE(ss.str() == "#");

				ss.str(std::string());

				hist.ToOstream(ss, 1, kCountPerGrad);
				REQUIRE(ss.str() == "");
			}
		}

		WHEN("a value of 3 is added, 3 times to the histogram")
		{
			hist.UpdateAddValue(3);
			hist.UpdateAddValue(3);
			hist.UpdateAddValue(3);

			THEN("the second bin should have a value of 3, and # should print for that bin")
			{
				REQUIRE(hist.histogram_ == std::vector<uint64_t>({0, 3}));

				std::stringstream ss;

				hist.ToOstream(ss, 0, kCountPerGrad);
				REQUIRE(ss.str() == "");

				ss.str(std::string());

				hist.ToOstream(ss, 1, kCountPerGrad);
				REQUIRE(ss.str() == "#");
			}
		}

		WHEN("a value of 3 is added, 4 times to the histogram")
		{
			hist.UpdateAddValue(3);
			hist.UpdateAddValue(3);
			hist.UpdateAddValue(3);
			hist.UpdateAddValue(3);

			THEN("the second bin should have a value of 4, and ## should print for that bin")
			{
				REQUIRE(hist.histogram_ == std::vector<uint64_t>({0, 4}));

				std::stringstream ss;

				hist.ToOstream(ss, 0, kCountPerGrad);
				REQUIRE(ss.str() == "");

				ss.str(std::string());

				hist.ToOstream(ss, 1, kCountPerGrad);
				REQUIRE(ss.str() == "##");
			}
		}

		WHEN("a value of 3 is added, 2 times to the histogram")
		{
			hist.UpdateAddValue(3);
			hist.UpdateAddValue(3);

			THEN("the second bin should have a value of 4, and # should print for that bin")
			{
				REQUIRE(hist.histogram_ == std::vector<uint64_t>({0, 2}));

				std::stringstream ss;

				hist.ToOstream(ss, 0, kCountPerGrad);
				REQUIRE(ss.str() == "");

				ss.str(std::string());

				hist.ToOstream(ss, 1, kCountPerGrad);
				REQUIRE(ss.str() == "#");
			}
		}
	}
}
#endif
