#include <stdlib.h>
#include <dlfcn.h>
#include <stdio.h>

#include <csignal>
#include <iostream>
/// This file contains the function signatures, for use with LD_PRELOAD, to override malloc, realloc, calloc and free.

#include <unordered_map>
#include <chrono>
#include <memory>

#include "MemoryAllocatorWithStats.h"

MemoryAllocatorWithStats mem_alloc_stats;

/// Prints the memory allocations stats to cerr stream when the appropriate signal is captured.
void SignalHandler(int signal)
{
	std::cerr << mem_alloc_stats << std::endl;
}

/// Called when the library is loaded, it sets the capturing of the SIGQUIT signal.
void __attribute__((constructor)) mem_alloc_stats_init()
{
	std::signal(SIGQUIT, SignalHandler);
}

void __attribute__((destructor)) mem_alloc_stats_deinit()
{
}

void* malloc (size_t size)
{
	return mem_alloc_stats.malloc(size);
}

void* calloc(size_t nmemb, size_t size)
{
	return mem_alloc_stats.calloc(nmemb, size);
}

void* realloc(void* ptr, size_t size)
{
	return mem_alloc_stats.realloc(ptr, size);
}

void free (void* ptr)
{
	mem_alloc_stats.free(ptr);
}
