#include <stdlib.h>

#include <memory>
#include <csignal>
#include <iostream>
#include <thread>
#include <vector>

int main ( void )
{
	std::cout << "int_a = malloc int" << std::endl;
	int* a = (int*)malloc(sizeof(int));

	std::cout << "sleep 2s" << std::endl;
	std::this_thread::sleep_for(std::chrono::seconds(2));

	std::raise(SIGQUIT);std::cerr << std::endl;

	std::cout << "char_b = realloc a to char" << std::endl;
	char* b = (char*)realloc(a, sizeof(char));

	std::raise(SIGQUIT);std::cerr << std::endl;

	std::cout << "free char_b" << std::endl;
	free(b);

	std::raise(SIGQUIT);std::cerr << std::endl;

	std::cout << "int4_c = calloc int, 4x" << std::endl;
	int* c = (int*)calloc(4, sizeof(int));

	std::raise(SIGQUIT);std::cerr << std::endl;

	std::cout << "free int4_c" << std::endl;
	free(c);

	std::raise(SIGQUIT);std::cerr << std::endl;

	std::cout << "char_mallocs = malloc 100000 char" << std::endl;
	std::cout << "b128_mallocs = malloc 100000 128-bytes" << std::endl;
	const int kMallocsCount = 25000;

	std::vector<void*> char_mallocs;
	char_mallocs.reserve(kMallocsCount);

	std::vector<void*> b128_mallocs;
	b128_mallocs.reserve(kMallocsCount);

	for(int i = 1; i <= kMallocsCount; i++)
	{
		char_mallocs.push_back(malloc(sizeof(char)));
		b128_mallocs.push_back(malloc(128));

		std::this_thread::sleep_for(std::chrono::milliseconds(1));

		if(i % 1000 == 0)
		{
			std::raise(SIGQUIT);std::cerr << std::endl;
		}
	}

	std::cout << "free char_mallocs" << std::endl;
	std::cout << "free b128_mallocs" << std::endl;
	for(int i = 1; i <= kMallocsCount; i++)
	{
		free(char_mallocs[i-1]);
		free(b128_mallocs[i-1]);

		std::this_thread::sleep_for(std::chrono::milliseconds(1));

		if(i % 1000 == 0)
		{
			std::raise(SIGQUIT);std::cerr << std::endl;
		}
	}
}
