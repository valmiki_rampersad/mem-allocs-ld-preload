#ifndef MEMORY_ALLOCATOR_WITH_STATS_
#define MEMORY_ALLOCATOR_WITH_STATS_

#include "SystemMemoryAllocator.h"
#include "PointerInfo.h"
#include "Histogram.h"

#include <unordered_map>
#include <vector>
#include <chrono>
#include <ratio>
#include <algorithm>
#include <stdexcept>
#include <limits>
#include <iostream>
#include <sstream>

/// This class wraps SystemMemoryAllocator and tracks the current allocations and their age.
/// It tracks the allocations for malloc, realloc, calloc and free. It supports writing the allocations and age,
/// as histograms, to a stream.
class MemoryAllocatorWithStats
{
public:
	MemoryAllocatorWithStats() :
		kMemAllocsBins({0, 4, 8, 16, 32, 64, 128, 256, 512, 1024, 2048, 4096, std::numeric_limits<uint64_t>::max()}),
		kHistogramCountPerGraduation(8123),
		kMemAllocsAgeBins({0, 1, 10, 100, 1000, std::numeric_limits<uint64_t>::max()})
	{
		total_num_allocs_ = 0;
		total_current_mem_allocs_ = 0;
		mem_allocs_hist_.SetBins(kMemAllocsBins);
		mem_allocs_age_hist_.SetBins(kMemAllocsAgeBins);
		sys_mem_alloc_hook_ = true;
	}

	~MemoryAllocatorWithStats() = default;

	/// Gets the total number of allocations since the start.
	uint64_t GetTotalNumAllocations() const
	{
		return total_num_allocs_;
	}

	/// Gets the current size of memory allocated, in bytes.
	uint64_t GetCurrentMemoryAllocation() const
	{
		return total_current_mem_allocs_;
	}

	/// Used to disable the tracking of the memory allocations.
	/// This is needed to prevent a recursive call loop, as the tracking ops itself would need to do memory allocations.
	void DisableMemAllocHook() const
	{
		sys_mem_alloc_hook_ = false;
	}

	/// Enables the tracking of memory allocations.
	void EnableMemAllocHook() const
	{
		sys_mem_alloc_hook_ = true;
	}

	void* malloc(size_t size)
	{
		void* ret_ptr = sys_mem_alloc_.malloc(size);
		UpdateStats(ret_ptr, size);
		return ret_ptr;
	}

	void* calloc(size_t nmemb, size_t size)
	{
		void* ret_ptr = sys_mem_alloc_.calloc(nmemb, size);
		UpdateStats(ret_ptr, nmemb * size);
		return ret_ptr;
	}

	void* realloc(void* ptr, size_t size)
	{
		// This is inefficient - only need to change the current total allocation
		// But it is cleaner and convenient this way.
		// Remove info for current ptr first.
		UpdateStats(ptr, 0);

		void* ret_ptr = sys_mem_alloc_.realloc(ptr, size);
		UpdateStats(ret_ptr, size);

		return ret_ptr;
	}

	void free(void* ptr)
	{
		UpdateStats(ptr, 0);
		sys_mem_alloc_.free(ptr);
	}

	/// Writes/prints the memory allocation histogram to a stream.
	/// \param os The stream to which to write.
	/// \returns The reference to the stream.
	std::ostream& AllocationHistogramToStream(std::ostream& os) const
	{
		os << "Current allocations by size: ( # = "<< kHistogramCountPerGraduation
				<< " current allocations)" << std::endl;

		auto num_bins = mem_allocs_hist_.GetNumBins();

		for(uint64_t bin_idx = 0; bin_idx < num_bins; bin_idx++)
		{
			// For the last element, print a different label
			if((kMemAllocsBins[bin_idx + 1] == std::numeric_limits<uint64_t>::max()) &&
					(bin_idx == (num_bins - 1)))
			{
				os << kMemAllocsBins[bin_idx] << " + bytes: ";
			}
			else
			{
				os << kMemAllocsBins[bin_idx] << " - " << kMemAllocsBins[bin_idx + 1] << " bytes: ";
			}
			mem_allocs_hist_.ToOstream(os, bin_idx, kHistogramCountPerGraduation);
			os << std::endl;
		}

		return os;
	}

	/// Writes/prints the memory allocation age histogram to a stream.
	/// It should be noted that this builds the age allocation histogram, every time it is called.
	/// \param os The stream to which to write.
	/// \returns The reference to the stream.
	std::ostream& AllocationAgeHistogramToStream(std::ostream& os) const
	{
		os << "Current allocations by age: ( # = " << kHistogramCountPerGraduation
				<< " current allocations)" << std::endl;

		BuildAllocationAgeHistogram();

		auto num_bins = mem_allocs_age_hist_.GetNumBins();

		for(uint64_t bin_idx = 0; bin_idx < num_bins; bin_idx++)
		{
			// For the last element, print a different label
			if(kMemAllocsAgeBins[bin_idx + 1] == std::numeric_limits<uint64_t>::max() &&
					bin_idx == (num_bins-1))
			{
				os << "> " << kMemAllocsAgeBins[bin_idx] << "sec: ";
			}
			else
			{
				os << "< " << kMemAllocsAgeBins[bin_idx + 1] << "sec: ";
			}
			mem_allocs_age_hist_.ToOstream(os, bin_idx, kHistogramCountPerGraduation);
			os << std::endl;
		}

		return os;
	}


private:
	/// Called to track the information for a given pointer.
	/// It stores the pointer, its size and its approximate allocation time. It also updates the total number of
	/// allocations from the start; the current memory allocations; and updates the memory allocation histogram. This
	/// also contains the safe-guards for recursive calls to malloc, realloc, calloc and free.
	/// \param ptr The pointer
	/// \param size The size of memory allocated and pointed to by the pointer. Passing a size of 0 removes tracking of
	/// the given pointer.
	/// \param alloc_time The approximate allocation time of the pointer. As this is called write after allocation, it
	/// has a default value of now().
	void UpdateStats(const void* ptr, const size_t size, const steady_clock_time& alloc_time = steady_clock::now())
	{
		// If a recursive call, then do not hook
		if(!sys_mem_alloc_hook_)
		{
			return;
		}

		if(!ptr)
		{
			return;
		}

		// To account for recursive calls to the memory allocation functions.
		sys_mem_alloc_hook_ = false;

		if(size == 0)
		{
			total_current_mem_allocs_ -= mem_allocs_[ptr].size;
			mem_allocs_hist_.UpdateRemoveValue(mem_allocs_[ptr].size);
			mem_allocs_.erase(ptr);

			sys_mem_alloc_hook_ = true;
			return;
		}

		total_num_allocs_++;
		total_current_mem_allocs_ += static_cast<uint64_t>(size);
		mem_allocs_hist_.UpdateAddValue(size);
		mem_allocs_[ptr] = PointerInfo(size, alloc_time);

		sys_mem_alloc_hook_ = true;
	}

	/// Builds the allocation age histogram. This is only called when the histogram is needed.
	void BuildAllocationAgeHistogram() const
	{
		mem_allocs_age_hist_.ResetCounts();
		auto time_now = steady_clock::now();

		for(auto ptr_info : mem_allocs_)
		{
			auto age = time_now - ptr_info.second.alloc_time;
			mem_allocs_age_hist_.UpdateAddValue(
					static_cast<uint64_t>(std::chrono::duration_cast<std::chrono::seconds>(age).count())
					);
		}
	}

	uint64_t total_num_allocs_;
	uint64_t total_current_mem_allocs_;

	std::unordered_map<const void*, PointerInfo> mem_allocs_;

	SystemMemoryAllocator sys_mem_alloc_;

	Histogram mem_allocs_hist_;
	const std::vector<uint64_t> kMemAllocsBins;
	const uint64_t kHistogramCountPerGraduation;

	// TODO: Do not like the idea of having this mutable.
	mutable Histogram mem_allocs_age_hist_;
	const std::vector<uint64_t> kMemAllocsAgeBins;

	mutable bool sys_mem_alloc_hook_ = false;
};

/// Writes the memory allocation stats to a stream.
std::ostream& operator<<(std::ostream& os, const MemoryAllocatorWithStats& mem_alloc_stats)
{
	// To prevent this function affecting the stats while writing to stream.
	mem_alloc_stats.DisableMemAllocHook();

	std::ostringstream ss;
	ss << ">>>>>>>>>>>> ";
	std::time_t now = std::chrono::system_clock::to_time_t(std::chrono::system_clock::now());
	ss << std::ctime(&now);
	//ss << '\b' << '\b' << '\b' << '\b';
	ss.seekp(-1, std::ios_base::cur);
	ss << " <<<<<<<<<<<<" << std::endl;

	ss << "Overall stats:" << std::endl;
	ss << mem_alloc_stats.GetTotalNumAllocations() << " Overall allocations since start." << std::endl;
	ss << mem_alloc_stats.GetCurrentMemoryAllocation() << " bytes Current total allocated size" << std::endl;

	ss << std::endl;

	mem_alloc_stats.AllocationHistogramToStream(ss);

	ss << std::endl;

	mem_alloc_stats.AllocationAgeHistogramToStream(ss);

	os << ss.str();

	mem_alloc_stats.EnableMemAllocHook();

	return os;
}

#endif //MEMORY_ALLOCATOR_WITH_STATS_
