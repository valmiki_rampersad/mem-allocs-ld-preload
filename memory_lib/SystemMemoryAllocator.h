#ifndef SYSTEM_MEMORY_ALLOCATOR_H_
#define SYSTEM_MEMORY_ALLOCATOR_H_

#include <dlfcn.h>

#include <iostream>
#include <stdexcept>

class SystemMemoryAllocator
{
public:
	SystemMemoryAllocator()
	{
		system_malloc = (void*(*)(size_t))dlsym(RTLD_NEXT, "malloc");
		system_calloc = (void*(*)(size_t, size_t))dlsym(RTLD_NEXT, "calloc");
		system_realloc = (void*(*)(void*, size_t))dlsym(RTLD_NEXT, "realloc");
		system_free = (void(*)(void*))dlsym(RTLD_NEXT, "free");
	}

	~SystemMemoryAllocator() = default;

	void* malloc ( size_t size )
	{
		if(!system_malloc)
		{
			// To try and behave like malloc?
			// Or maybe we should just crash here?
			return nullptr;
		}
		return (*system_malloc)(size);
	}

	void* calloc (size_t nmemb, size_t size)
	{
		if(!system_calloc)
		{
			return nullptr;
		}
		return (*system_calloc)(nmemb, size);
	}

	void* realloc(void* ptr, size_t size)
	{
		if(!system_realloc)
		{
			return nullptr;
		}
		return (*system_realloc)(ptr, size);
	}

	void free (void* ptr)
	{
		if(!system_free)
		{
			return;
		}
		(*system_free)(ptr);
	}

private:
	void* (*system_malloc)(size_t);
	void* (*system_calloc)(size_t, size_t);
	void* (*system_realloc)(void*, size_t);
	void (*system_free)(void*);
};

#endif
